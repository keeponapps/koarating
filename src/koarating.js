/*
 * koarating
 *
 *
 * Copyright (c) 2012 Aleksandar Gosevski
 * Licensed under the MIT, GPL licenses.
 */

(function($) {

    var namespace = 'ratings';

    function Rating(element, options) {
        var o = this.options = $.extend({}, $.fn.rating.defaults, options);

        this.$element = $(element);
        this.$array = [];
        this.half = false;
        this.rate = parseFloat(this.$element.data('rating'));
        this.integer = parseInt(this.rate, 10);
        this.decimal = this.rate - this.integer;

        // Push divs to an array
        for (var i = o.max - 1; i >= 0; i--) {
            this.$array.push('<div />');
        }

        this.$element.append(this.$array.join(''));
        //this.$element.text('rate: '+this.rate+'integer: '+this.integer+'decimal: '+this.decimal);

        this.half = (this.decimal > 0.4 && this.decimal < 0.6) ? true : false;

        for (i = this.integer - 1; i >= 0; i--) {
            this.$element.find('div').eq(i).addClass(o.fullClass);
        }

        if (this.half === true) {
            this.$element.find('div').eq(this.integer).addClass(o.halfClass);
        }

        this.$element.find('div').not('.'+o.fullClass).not('.'+o.halfClass).addClass(o.blankClass);
    }

    $.fn.rating = function (option) {
        return this.each(function () {
            var $this = $(this),
            data = $this.data(namespace),
            options = typeof option === 'object' && option;

            if ( !data ) {
                $this.data(namespace, (data = new Rating(this, options)));
            }

            if ( typeof option === 'string' && option.charAt(0) !== '_') {
                data[option]();
            }
        });
    };

    $.fn.rating.Constructor = Rating;

    $.fn.rating.defaults = {
        max : 5,
        fullClass : 'full-star',
        halfClass : 'half-star',
        blankClass : 'blank-star'
    };

}(jQuery));
